with Text_IO; 
use Text_IO;
procedure Hello is
   type MyInteger is new Integer;
   type Date is record
      Day:Integer;
      Month:Integer;
   end record;
   type Date_Acc is access Date;
   type MyRange is range 1..5;
   type MyArray is
      Array(MyRange) of Integer;
   I:Integer:=1;
   V:Integer;
   J:MyInteger:=1;
   B:Date:=(5,3);
   B3:Date_Acc:=new Date;
   B4:Date_Acc:=B3;
   B2:Date:=(Day=>5,Month=>3);
   Arr:MyArray:=(1,2,3,4,5);
   procedure Print_message is
      begin
         Put_line("Alain is nice");
      end Print_message;
   procedure Print_int(I:Integer) is   
   begin
      Put_Line(Integer'Image(I));
   end Print_int;
   procedure Add1inout(I: in out Integer) is
   begin
      I:=I+1;
   end Add1inout;
   function Add1(I:Integer) return Integer is
   begin
      return (I+1);
   end Add1;
begin
   if 1 > 0 then
      Put_Line("Hello world!");
   else
      Put_Line("Error");
   end if;
   for I in 1..5 loop 
      Put_Line("Hi");
   end loop;
   loop
      exit when I=5;
      put_Line("There");
      I:=I+1;
   end loop;
   Print_message ;
   Print_int(5);
   Print_int(Add1(5));
   I:=5;
   Add1inout(i);
   Print_int(i);
   Print_int(B.Day);
   for I in MyRange loop
      V:=Arr(I);
      Print_int(V);
   end loop;
end Hello;
